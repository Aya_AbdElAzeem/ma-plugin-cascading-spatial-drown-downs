const path = require('path');

const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const OUTPUT_PATH = (process.env.OUTPUT_PATH && path.resolve(process.env.OUTPUT_PATH)) || path.join(__dirname, 'dist/test_app');
const NODE_ENV = process.env.NODE_ENV || 'production';
const ASSET_PATH = process.env.ASSET_PATH || '/';

module.exports = {
    mode: NODE_ENV,
    entry: './test_app/index.js',
    output: {
        filename: 'bundle.js',
        path: OUTPUT_PATH,
        publicPath: ASSET_PATH,
        libraryTarget: 'umd'
    },
    devServer: {
        port: 3001,
        contentBase: OUTPUT_PATH,
        writeToDisk: true,
        historyApiFallback: true,
        headers: {
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE, PATCH, OPTIONS",
            "Access-Control-Allow-Headers": "X-Requested-With, content-type, Authorization",
            "Access-Control-Allow-Credentials": true
        },
        proxy: {
            '/geoserver': {
                target: 'http://3.220.123.110:8080',
                changeOrigin: true
            }
        },
        clientLogLevel: 'debug'
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                use: "babel-loader",
                exclude: /node_modules/
            },
            {
                test: /\.css$/,
                use: ["style-loader", "css-loader"]
            },
            {
                test: /\.(png|jpg|gif)$/,
                use: [
                  {
                    loader: "url-loader"
                  },
                ],
            },
            {
                test: /\.(ttf|otf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?|(jpg|gif)$/,
                loader: 'file-loader'
            }
        ]
    },
    plugins: [
        new CleanWebpackPlugin(),
        new HtmlWebpackPlugin({
            template: './test_app/index.html'
        })
    ]
};
/**
 * Author: Amr Samir
 * 
 * Description: 
 *  - This index file exports plugin's components and/or reducers and/or actions.
 */

import CascadingSpatialDrowndownsComponent from './components/CascadingSpatialDrowndowns/CascadingSpatialDrowndowns.component';


const components = [
    CascadingSpatialDrowndownsComponent,
   
];

export { components };
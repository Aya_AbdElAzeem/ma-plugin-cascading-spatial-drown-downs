/**
 * Author: Amr Samir
 * 
 * Description: 
 *  - An example of a plugin that listens to another 
 *    plugin's state changes (Map plugin), and log that state.
 */


import React from 'react';
import {connect} from 'react-redux';
import { selectorsRegistry } from 'ma-lib';

import './style.css';

import { request } from 'ma-lib';

import {setMapBBOX} from 'ma-plugin-ol-map/src/actions/mapActions.js'

class CascadingSpatialDrowndownsComponent extends React.Component {
        
    constructor(props) {
        super(props);
        this.state={
            features:[],
            sectorFeatures:[],
            subSectorFeatures:[],
            selectionstatID:'',
            selectionSectorID:'',
            

        }
        this.getstate=this.getstate.bind(this);
        this.getsectors=this.getsectors.bind(this);
        this.getSubSectors=this.getSubSectors.bind(this)
    }
    
    

    componentDidMount() {
        request.get('/geoserver/msd/wfs?service=wfs&version=2.0.0&request=GetFeature&typeName=msd:admin_boundaries_gov&propertyName=gov_code,name_en,name_ar&outputFormat=application/json').then((res) => {
            this.setState({
                features:res.data.features
            })

        });
        request.get('/geoserver/msd/wfs?service=wfs&version=2.0.0&request=GetFeature&typeName=msd:admin_boundaries_sectors&propertyName=gov_code,sec_name_a,sec_code&outputFormat=application/json').then((ressector) => {
            this.setState({
                sectorFeatures:ressector.data.features
            })

        });
        request.get('/geoserver/msd/wfs?service=wfs&version=2.0.0&request=GetFeature&typeName=msd:admin_boundaries_subsectors&propertyName=geom,ssec_code,ssec_name_Ar,sec_code&outputFormat=application/json').then((ressub) => {
            this.setState({
                subSectorFeatures:ressub.data.features
            })

        });

    }

    /**
     * Description: 
     *  - React lifecycle method, here we check for state changes.
     */
    

    render() {
        return (
            <div>
                
                {this.getstate()}
                {this.getsectors(this.state.selectionstatID)}
                {this.getSubSectors(this.state.selectionSectorID)}
            </div>
            
        );
    }
    getstate(){
        return(
          <div>
            
            <select onChange={(e)=>{
                
                this.setState({selectionstatID:e.target.value})
                }}>
              <option>select GOV</option>
              {this.state.features.map(fet=><option value={fet.properties.gov_code} >{fet.properties.name_ar}</option>)}
            </select>
          </div>
        )
    }
    getsectors(selectionstatID){
        if (this.state.selectionstatID!==''){
          return(
            <div>
            <select onChange={(e)=>{
                
                this.setState({selectionSectorID:e.target.value})
                
                }}>
              <option>select SECTOR</option>
              {
              this.state.sectorFeatures.map(fet=>{if(fet.properties.gov_code===selectionstatID){
                  return(<option value={fet.properties.sec_code} >{fet.properties.sec_name_a}</option>)
                
              }
    
              }
              )}
            </select>
          </div>
          )
        }
    
    }
    getSubSectors(selectionSectorID){
        if (this.state.selectionSectorID!=='' ){
          return(
            <div>
            
            <select onChange={(e)=>{
              this.state.subSectorFeatures.map(elem=>{if(elem.properties.ssec_code===e.target.value){
                this.props.setMapBBOX(elem.properties.bbox)
              }})
              
              }}>
              <option>select SUBsector</option>
              {

                this.state.subSectorFeatures.map(fet=>{if(fet.properties.sec_code===selectionSectorID){
                return(<option value={fet.properties.ssec_code} >{fet.properties.ssec_name_Ar}</option>)}
                }
                )
              }
            </select>
          </div>
          )
        }
    
      }

    
    
      

}

const mapStateToProps = (state, ownProps) => {
    return {
        map: selectorsRegistry.getSelector('selectMapReducers', state, ownProps.reducerId)
    }
};

export default connect(mapStateToProps,{setMapBBOX})(CascadingSpatialDrowndownsComponent);


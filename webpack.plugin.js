const path = require('path');

const OUTPUT_PATH = (process.env.OUTPUT_PATH && path.resolve(process.env.OUTPUT_PATH)) || path.join(__dirname, 'dist/build');
const NODE_ENV = process.env.NODE_ENV || 'production';
const ASSET_PATH = process.env.ASSET_PATH || '/';

const DEV_TOOL = NODE_ENV == "production" ? "source-map" : "inline-source-map";

module.exports = {
    mode: NODE_ENV,
    devtool: DEV_TOOL,
    entry: './src/index.js',
    output: {
        filename: 'dropdown.js',
        path: OUTPUT_PATH,
        publicPath: ASSET_PATH,
        libraryTarget: 'umd',
        library: 'dropdown'
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                use: "babel-loader",
                exclude: /node_modules/
            },
            {
                test: /\.css$/,
                use: ["style-loader", "css-loader"]
            },
            {
                test: /\.(png|jpg|gif)$/,
                use: [
                  {
                    loader: 'file-loader',
                    options: {
                        name: 'assets/icons/[name].[ext]',
                        outputPath: ASSET_PATH
                    },
                  },
                ],
            },
        ]
    },
    externals: {
        'react': 'react',
        'react-dom': 'react-dom',
        'redux': 'redux',
        'react-redux': 'react-redux',
        'ma-lib': 'ma-lib'
    }
};
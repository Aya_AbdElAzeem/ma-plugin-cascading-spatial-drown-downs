import React from 'react';
import ReactDOM from 'react-dom';
import { Provider, connect } from "react-redux";
import { createStore, combineReducers, applyMiddleware } from 'redux';
import loggerMiddleware from 'redux-logger';
import thunkMiddleware from 'redux-thunk';
import { initPlugin, reducerRegistry } from 'ma-lib';
import ReactAce from 'react-ace-editor';

import './style.css';

import * as mapPlugin from 'ma-plugin-ol-map';
const MapComponent = mapPlugin.components[0];
initPlugin(mapPlugin);

import {
    components as targetComponents
} from '../src/index';
const TargetComponent = targetComponents[0];


import map_settings from './map_settings';
import test_settings from './test_settings';

class App extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            targetSelected: false
        };

        this.props.initMapReducer(map_settings);
        this.state = {
            settings_text: JSON.stringify(test_settings, null, 2),
            settings_json: test_settings
        };
    }

    applySettings() {
        try {
            var settings_json = JSON.parse(this.state.settings_text);
            this.setState({
                settings_json
            });
        }
        catch(e) {
            alert(e.message);
        }
    }

    render() {
        return (
            <div className="page">
                <h1 className="title">drop Sandbox</h1>
                <div className="container">
                    <div className="editor">
                        <div className="editor-title-container">
                            <h2 className="editor-title">Setting Editor</h2>
                            <input className="editor-run-button" type="button" value="RUN" onClick={this.applySettings.bind(this)} />
                        </div>
                        <ReactAce
                            setValue={this.state.settings_text}
                            mode="json"
                            theme="monokai"
                            setReadOnly={false}
                            onChange={(e) => {this.setState({settings_text: e})}}
                            style={{ flex: '1' }} />    
                    </div>

                    <div className="map">
                        <MapComponent />
                    </div>

                    <div 
                        className={"plugin-container " + (this.state.targetSelected ? "plugin-selected" : "") } 
                        onClick={() => {this.setState({targetSelected: !this.state.targetSelected})}}>
                        <TargetComponent isActive={this.state.targetSelected} settings={this.state.settings_json} />
                    </div>
                </div>
            </div>
        );
    }
}

const mapDispatchToProps = dispatch => {
    return {
        initMapReducer: (settings) => dispatch(mapPlugin.actions.initMapReducer(settings))
    }
}

const ConnectedApp = connect(null, mapDispatchToProps)(App);

const store = createStore(combineReducers(reducerRegistry.getReducers()), {}, applyMiddleware(thunkMiddleware, loggerMiddleware));

ReactDOM.render(
    <Provider store={store}>
        <ConnectedApp />
    </Provider>, 
    document.getElementById("app"));

